package Main;

import Data.WeatherData;
import Graphic.BarChart3D;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

public class Main {


	public static void main(String[] args) {

		File inputFile = new File(jOptionPanelToRead());
		File outputFile = new File("parseWeatherInformation.txt");

		ArrayList<String> info = getText(inputFile);

		if (info!=null){
			WeatherData weatherData = new WeatherData(parse(info));

			try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile))){
				weatherData.getDayArrayList().forEach(x-> {
					try {
						bufferedWriter.write(String.valueOf(x));
						bufferedWriter.newLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

				bufferedWriter.newLine();
				bufferedWriter.write(weatherData.toString());
				BarChart3D graphic = new BarChart3D("Weather Information","Days","Value",weatherData.getDayArrayList());

				graphic.showGrath();
				Thread.sleep(1000);
				ImageIO.write(graphic.getGrathImage(),"png", new File("graphImage.png"));

			} catch (Exception e) {
				System.out.println("restart please");
			}
		}else {
			System.out.println("Null pointer exception");
		}
		System.out.println("all good work)");
	}

	private static String jOptionPanelToRead() {
		return JOptionPane.showInputDialog(

				"Please write your FULL way to info(file)");
	}

	private static ArrayList<String> parse(ArrayList<String> info) {
		//убираем пока что не нужную информацию
		while (!info.get(0).contains("T0000")){
			info.remove(0);
		}
		return info;
	}

	private static ArrayList<String> getText(File file) {
		ArrayList<String> strings = null;
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
			String s = bufferedReader.readLine();
			strings = new ArrayList<>();
			while (s!=null){
				strings.add(s);
				s = bufferedReader.readLine();
			}
		} catch (IOException e) {
			System.out.println("No Text");
		}
		return strings;
	}
}

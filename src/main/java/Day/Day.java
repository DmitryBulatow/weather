package Day;

import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.Calendar;

public class Day
{
    private Calendar date;
    private ArrayList<Double> temperatureByDay;
    private double maxTemperature;
    private int maxTemperatureTime;
    private double averageTemperature;
    private ArrayList<Double> humidityByDay;
    private double minHumidity;
    private Integer minHumidityTime;
    private double averageHumidity;
    private ArrayList<Double> windSpeedByDay;
    private double maxWindSpeed;
    private int maxWindSpeedTime;
    private double averageWindSpeed;
    private ArrayList<Double> windOrientationByDay;
    private WindOrientation windOrientation;

	public WindOrientation getWindOrientation() {
		return windOrientation;
	}

	public Day(ArrayList<String> strings) {
        this.date = this.parseStringsDate(strings);
        this.temperatureByDay = this.parseBeforeComa(strings);
        this.humidityByDay = this.parseBeforeComa(strings);
        this.windSpeedByDay = this.parseBeforeComa(strings);
        this.windOrientationByDay = this.parseBeforeComa(strings);
        this.maxTemperature = this.searchMaxFromInfoByDay(this.temperatureByDay);
        this.maxTemperatureTime = this.searchTime(this.maxTemperature, this.temperatureByDay);
        this.averageTemperature = this.searchAverage(this.temperatureByDay);
        this.minHumidity = this.searchMinFromInfoByDay(this.humidityByDay);
        this.minHumidityTime = this.searchTime(this.minHumidity, this.humidityByDay);
        this.averageHumidity = this.searchAverage(this.humidityByDay);
        this.maxWindSpeed = this.searchMaxFromInfoByDay(this.windSpeedByDay);
        this.maxWindSpeedTime = this.searchTime(this.maxWindSpeed, this.windSpeedByDay);
        this.averageWindSpeed = this.searchAverage(this.windSpeedByDay);
        this.windOrientation = this.searchAverageOrientation();
    }
    
    private int searchTime(double value, ArrayList<Double> informationByDay) {
        for (int i = 0; i < informationByDay.size(); ++i) {
            if (value == informationByDay.get(i)) {
                return i;
            }
        }
        return -1;
    }
    
    private WindOrientation searchAverageOrientation() {
        final double average = this.searchAverage(this.windOrientationByDay);
        if (average >= 22.5 && average <= 67.5) {
            return WindOrientation.NORTHEAST;
        }
        if (average >= 67.5 && average <= 112.5) {
            return WindOrientation.EAST;
        }
        if (average >= 112.5 && average <= 157.5) {
            return WindOrientation.SOUTHEAST;
        }
        if (average >= 157.5 && average <= 202.5) {
            return WindOrientation.SOUTH;
        }
        if (average >= 202.5 && average <= 247.5) {
            return WindOrientation.SOUTHWEST;
        }
        if (average >= 247.5 && average <= 292.5) {
            return WindOrientation.WEST;
        }
        if (average >= 292.5 && average <= 337.5) {
            return WindOrientation.NORTHWEST;
        }
        return WindOrientation.NORTH;
    }
    
    private double searchMaxFromInfoByDay(ArrayList<Double> informationByDay) {
        double max = Double.MIN_VALUE;
        for (int i = 0; i < informationByDay.size(); ++i) {
            if (max < informationByDay.get(i)) {
                max = informationByDay.get(i);
            }
        }
        return max;
    }
    
    private double searchAverage(ArrayList<Double> informationByDay) {
        double average = 0.0;
        for (int i = 0; i < informationByDay.size(); ++i) {
            average += informationByDay.get(i);
        }
        return average / informationByDay.size();
    }
    
    private double searchMinFromInfoByDay(ArrayList<Double> humidityByDay) {
        double min = Double.MAX_VALUE;
        for (int i = 0; i < humidityByDay.size(); ++i) {
            if (min > humidityByDay.get(i)) {
                min = humidityByDay.get(i);
            }
        }
        return min;
    }
    
    private ArrayList<Double> parseBeforeComa(ArrayList<String> strings) {
        final ArrayList<Double> arrayList = new ArrayList<Double>();
        for (int i = 0; i < strings.size(); ++i) {
        	String str = strings.get(i).split(",")[0];
            arrayList.add(Double.valueOf(str));
            strings.set(i,strings.get(i).replaceFirst(str+",", ""));
        }
        return arrayList;
    }
    
    private Calendar parseStringsDate(ArrayList<String> strings) {
        int year = 0;
        for (int i = 0; i < 4; ++i) {
            year += Character.digit(strings.get(0).charAt(i), 10);
            year *= 10;
        }
        year /= 10;
        int month = 0;
        month += Character.digit(strings.get(0).charAt(4), 10) * 10;
        month += Character.digit(strings.get(0).charAt(5), 10);
        int day = 0;
        day += Character.digit(strings.get(0).charAt(6), 10) * 10;
        day += Character.digit(strings.get(0).charAt(7), 10);
        final Calendar date = new GregorianCalendar(year, month, day);
        for (int j = 0; j < strings.size(); ++j) {
            strings.set(j, strings.get(j).substring(14));
        }
        return date;
    }
    
    @Override
    public String toString() {
        return "Day: " + this.date.get(Calendar.YEAR) + "." + this.date.get(Calendar.MONTH) + "." + this.date.get(Calendar.DATE) +"\n"+
		        "average temperature = " + String.format("%.2f",this.averageTemperature) +"°C"+
		        ", max temperature = " + this.maxTemperature + "°C"+
		        ", recorded in " + this.maxTemperatureTime +" o'clock\n"+
		        "average humidity = " + String.format("%.2f",this.averageHumidity) + "%"+
		        ", min humidity = " + String.format("%.2f",this.minHumidity) +"%"+
		        ", recorded in " + this.minHumidityTime + " o'clock\n"+
		        "average wind speed = " + String.format("%.2f",this.averageWindSpeed) + " km/h"+
		        ", max wind speed = " + this.maxWindSpeed +" km/h"+
		        ", recorded in " + this.maxWindSpeedTime +  " o'clock"+
		        ", average wind orientation = " + this.windOrientation + "°";
    }

	public double getMaxTemperature() {
		return maxTemperature;
	}

	public int getMaxTemperatureTime() {
		return maxTemperatureTime;
	}

	public double getMinHumidity() {
		return minHumidity;
	}

	public Integer getMinHumidityTime() {
		return minHumidityTime;
	}

	public double getMaxWindSpeed() {
		return maxWindSpeed;
	}

	public int getMaxWindSpeedTime() {
		return maxWindSpeedTime;
	}

	public String getDateToString() {
		return this.date.get(Calendar.YEAR) + "." + this.date.get(Calendar.MONTH) + "." + this.date.get(Calendar.DATE);
	}

	public Calendar getDate() {
		return date;
	}

	public double getAverageTemperature() {
		return this.averageTemperature;
	}

	public double getAverageHumidity() {
		return this.averageHumidity;
	}

	public double getAverageWindSpeed() {
		return this.averageWindSpeed;
	}
}

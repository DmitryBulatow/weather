package Day;

public enum WindOrientation
{
    NORTH, 
    NORTHEAST, 
    EAST, 
    SOUTHEAST, 
    SOUTH, 
    SOUTHWEST, 
    WEST, 
    NORTHWEST;
}

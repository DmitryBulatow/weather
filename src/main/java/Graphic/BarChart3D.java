package Graphic;

import Day.Day;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import java.awt.*;
import java.awt.image.RenderedImage;
import java.util.ArrayList;
//GOD OBJECT+-
public class BarChart3D extends ApplicationFrame {
	private static final long serialVersionUID = 1L;
	private JFreeChart chart;

	public BarChart3D(final String title,final  String xName,final  String yName,final  ArrayList<Day> days) {
        super(title);

		final CategoryDataset dataset = this.createDataset(days);
		final JFreeChart chart = createChart(dataset,title , xName, yName);
		ChartPanel chartPanel = new ChartPanel(chart);
		Toolkit.getDefaultToolkit().getScreenSize();
		chartPanel.setPreferredSize(new Dimension(1080,720));
		setContentPane(chartPanel);
	}


    private JFreeChart createChart(final CategoryDataset dataset, String title, String xName, String yName) {
        
        final JFreeChart chart = ChartFactory.createBarChart3D(
		    title,                     // chart title
		    xName,                     // domain axis label
            yName,                     // range axis label
            dataset,                   // data
            PlotOrientation.VERTICAL,  // orientation
            true,                // include legend
            true,               // tooltips
		        true                  // urls
        );
	    this.chart = chart;
        // Определение фона plot'a
	    CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint (new Color(240, 255, 252));

        // Настройка CategoryAxis 
        CategoryAxis axis = plot.getDomainAxis();
        axis.setLabelPaint(new Color(16, 0, 54));
		axis.setLabelFont(new Font("Arial", Font.BOLD,22));
	    ValueAxis rangeAxis = plot.getRangeAxis();
	    rangeAxis.setLabelFont(new Font("Arial", Font.BOLD,20));
	    rangeAxis.setLabelPaint(new Color(16, 0, 54));
	    // Скрытие осевых линий и меток делений
        axis.setAxisLineVisible (true);    // осевая линия
        axis.setTickMarksVisible(true);    // метки деления оси

	    // Наклон меток значений
        axis.setCategoryLabelPositions(
            CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 6.0)
        );

        CategoryItemRenderer renderer = plot.getRenderer();
        renderer.setSeriesPaint(0,new Color(255, 140, 0));
        renderer.setSeriesPaint(1,new Color(0, 166, 255));
        renderer.setSeriesPaint(2,new Color(0, 255, 216));
        renderer.setBaseItemLabelsVisible(true);
        BarRenderer r = (BarRenderer) renderer;
		r.setMaximumBarWidth(0.05);
		r.setAutoPopulateSeriesFillPaint(true);
        return chart;
    }

	public void showGrath(){
		pack();
		RefineryUtilities.centerFrameOnScreen(this);
        setVisible(true);
		setExtendedState(MAXIMIZED_BOTH);
	}
	public RenderedImage getGrathImage(){
		return chart.createBufferedImage(1920,1080);
	}

	public CategoryDataset createDataset(ArrayList<Day> days) {
		DefaultCategoryDataset result = new DefaultCategoryDataset();
		for (Day day : days) {
			result.addValue(day.getMaxTemperature(), "Max Temperature °C ", day.getDateToString());
			result.addValue(day.getMinHumidity(), "Min Humidity % ", day.getDateToString());
			result.addValue(day.getMaxWindSpeed(), "Max Wind Speed km/h ", day.getDateToString());
		}
		return result;
	}

}

package Data;


import Day.Day;
import Day.WindOrientation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;

public class WeatherData {
	private ArrayList<Day> dayArrayList;

	public WeatherData(ArrayList<String> text) {
		this.dayArrayList = this.createDays(text);
	}

	private ArrayList<Day> createDays(ArrayList<String> text) {
		final ArrayList<Day> days = new ArrayList<>();
		for (int i = 0; i < text.size() / 24; ++i) {
			final ArrayList<String> informationByDay = new ArrayList<>();
			for (int j = 0; j < 24; ++j) {
				informationByDay.add(text.get(i * 24 + j));
			}
			days.add(new Day(informationByDay));
		}
		return days;
	}

	public ArrayList<Day> getDayArrayList() {
		return this.dayArrayList;
	}

	public double getAverageWindSpeedByWeek() {
		final ArrayList<Double> averageWindSpeed = new ArrayList<>();
		for (Day day : this.dayArrayList) {
			averageWindSpeed.add(day.getAverageWindSpeed());
		}
		return this.getAverage(averageWindSpeed);
	}

	public double getAverageHumidityByWeek() {
		final ArrayList<Double> averageHumidity = new ArrayList<>();
		for (Day day : this.dayArrayList) {
			averageHumidity.add(day.getAverageHumidity());
		}
		return this.getAverage(averageHumidity);
	}

	public double getAverageTemperatureByWeek() {
		final ArrayList<Double> averageTemperature = new ArrayList<>();
		for (Day day : this.dayArrayList) {
			averageTemperature.add(day.getAverageTemperature());
		}
		return this.getAverage(averageTemperature);
	}

	public double getAverage(ArrayList<Double> averageInformation) {
		double average = 0.0;
		for (Double aDouble : averageInformation) {
			average += aDouble;
		}
		return average / averageInformation.size();
	}

	public Day getDayMaxHot() {
		Day day = dayArrayList.stream().max(Comparator.comparingDouble(o -> o.getMaxTemperature())).get();
		day.getDate().set(Calendar.HOUR_OF_DAY, day.getMaxTemperatureTime());
		return day;
	}

	public Day getDayWithMinHumidity() {
		Day day = dayArrayList.stream().min(Comparator.comparingDouble(o -> o.getMinHumidity())).get();
		day.getDate().set(Calendar.HOUR_OF_DAY, day.getMaxTemperatureTime());
		return day;
	}

	public Day getDayMaxWindiest() {
		Day day = dayArrayList.stream().max(Comparator.comparingDouble(x -> x.getMaxWindSpeed())).get();
		day.getDate().set(Calendar.HOUR_OF_DAY, day.getMaxTemperatureTime());
		return day;
	}

	public String getMostCommonWindOrientationToString(){
		HashMap<WindOrientation,Integer> hashMap = new HashMap<>();
		hashMap.put(WindOrientation.NORTH,0);
		hashMap.put(WindOrientation.EAST,0);
		hashMap.put(WindOrientation.SOUTH,0);
		hashMap.put(WindOrientation.WEST,0);
		hashMap.put(WindOrientation.NORTHEAST,0);
		hashMap.put(WindOrientation.NORTHWEST,0);
		hashMap.put(WindOrientation.SOUTHEAST,0);
		hashMap.put(WindOrientation.SOUTHWEST,0);
		for (Day day : dayArrayList) {
			hashMap.put(day.getWindOrientation(),
					hashMap.get(day.getWindOrientation()) + 1);
		}
		return "most often wind orientation: "+hashMap.keySet().stream().max(Comparator.comparingInt(hashMap::get)).get();
	}
	@Override
	public String toString() {
		return getAverageTemperatureToString() + "\n" +
				getHighestTemperatureToString() + "\n" +
				getLowestHumidityToString() + "\n" +
				getHighestWindSpeedToString() + "\n"+
				getMostCommonWindOrientationToString();
	}

	public String getAverageTemperatureToString() {
		return "Average Temperature For Week = " + String.format("%.2f", getAverageTemperatureByWeek()) + "\n" +
				"Average Humidity For Week = " + String.format("%.2f", getAverageHumidityByWeek()) + "\n" +
				"Average Wind Speed For Week = " + String.format("%.2f", getAverageWindSpeedByWeek()) + "\n\n";
	}

	public String getHighestTemperatureToString() {
		Day day = getDayMaxHot();
		return "The highest temperature was: " + day.getDate().get(Calendar.YEAR) +
				"." + day.getDate().get(Calendar.MONTH) +
				"." + day.getDate().get(Calendar.DATE) +
				" at " + day.getDate().get(Calendar.HOUR_OF_DAY) + " o'clock" +
				" = " + day.getMaxTemperature();
	}

	public String getHighestWindSpeedToString() {
		Day day = getDayMaxWindiest();
		return "The strongest wind was: " + day.getDate().get(Calendar.YEAR) +
				"." + day.getDate().get(Calendar.MONTH) + "." + day.getDate().get(Calendar.DATE) +
				" at " + day.getDate().get(Calendar.HOUR_OF_DAY) + " o'clock" +
				" = " + day.getMaxWindSpeed();
	}

	public String getLowestHumidityToString() {
		Day day = getDayWithMinHumidity();
		return "Lowest humidity was: " + day.getDate().get(Calendar.YEAR) +
				"." + day.getDate().get(Calendar.MONTH) + "." + day.getDate().get(Calendar.DATE) +
				" at " + day.getDate().get(Calendar.HOUR_OF_DAY) + " o'clock" +
				" = " + day.getMinHumidity();
	}
}
